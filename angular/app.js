var app = angular.module('facturacionApp', [ 
  'ngRoute', 
  'facturacionApp.configuracion',
  'facturacionApp.messages',
  'facturacionApp.notifications'
]);

app.controller('mainCtrl', [ '$scope', 'Configuracion', 'Mensajes', 'Notificaciones', function($scope, Configuracion, Mensajes, Notificaciones) {

  $scope.config = {};
  $scope.messages = Mensajes.messages;
  $scope.notifications = Notificaciones.notifications;
  $scope.user = {
    "name": "Miguel Herrero"
  }

  Configuracion.cargar()
    .then(function(config) {
      $scope.config = Configuracion.config;
    })
  
}]);

app.config([ '$routeProvider', function($routeProvider) {

  $routeProvider
    .when('/', {
      templateUrl: 'dashboard/dashboard.html'
    })
    .otherwise({
      redirectTo: '/'
    })
}]);

// ===================================
//     Filters
// ===================================
app.filter('quitarletra', function() {
  return function(palabra) {

    if (palabra) {
      if (palabra.length > 1) {
        return palabra.substr(1);
      } else {
        return palabra;
      }
    }
  }
})
.filter('mensajeCorto', function() {
  return function(message) {
    if (message) {
      if (message.length > 40) {
        return message.substr(0,40) + '…';
      } else {
        return message;
      }
    }
  }
})