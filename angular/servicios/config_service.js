var app = angular.module('facturacionApp.configuracion', []);

app.factory('Configuracion', ['$http', '$q', function($http, $q) {

  var self = {
    config: {},
    cargar: function() {
      var d = $q.defer();

      $http.get('config.json')
        .then(function successCallback(response) {
          self.config = response.data;
          d.resolve();
        }, function errorCallback(response) {
          d.reject();
          console.error('No se pudo cargar el archivo de configuración')
        })
      
      return d.promise;
    }
  };

  return self;
}]);