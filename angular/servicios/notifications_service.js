var app = angular.module('facturacionApp.notifications', []);

app.factory('Notificaciones', ['$http', '$q', function($http, $q) {

  var self = {
    notifications: [
      {
        "icon": "fa-user",
        "message": "New registered user"
      },
      {
        "icon": "fa-save",
        "message": "50 % disk usage"
      },
      {
        "icon": "fa-warning",
        "message": "New registered user"
      }
    ]
  };

  return self;
}]);