var app = angular.module('facturacionApp.messages', []);

app.factory('Mensajes', ['$http', '$q', function($http, $q) {

  var self = {
    messages: [
      {
        img: "dist/img/user1-128x128.jpg",
        name: "JUan Carlos",
        title: "Bienvenido a nuestro servicio de facturación",
        date: "5-agosto",
      },
      {
        img: "dist/img/user3-128x128.jpg",
        name: "María Fernández",
        title: "Bienvenido a nuestro servicio de facturación",
        date: "4-agosto",
      },
      {
        img: "dist/img/user5-128x128.jpg",
        name: "Silvia Varas",
        title: "Bienvenido a nuestro servicio de facturación",
        date: "1-agosto",
      }
    ]
  };

  return self;
}]);